var flowFile = session.get();
if (flowFile != null) {
  try {
    var StreamCallback =  Java.type("org.apache.nifi.processor.io.StreamCallback")
    var IOUtils = Java.type("org.apache.commons.io.IOUtils")
    var StandardCharsets = Java.type("java.nio.charset.StandardCharsets")
    var Base64 = Java.type("java.util.Base64")
    var Files = Java.type("java.nio.file.Files")
    var Paths = Java.type("java.nio.file.Paths")
    flowFile = session.write(flowFile,
      new StreamCallback(function(inputStream, outputStream) {
        var rootPathProp = context.getProperty("rootPath");
        if (rootPathProp.isExpressionLanguagePresent()) {
          rootPathProp = rootPathProp.evaluateAttributeExpressions();
        }
        var content = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
        var contentObj = JSON.parse(content);
        var filePath = rootPathProp.getValue() + contentObj.path;
        var text = Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(filePath)))
        contentObj.data = text;
        outputStream.write(JSON.stringify(contentObj).getBytes(StandardCharsets.UTF_8)) 
      }))
    
    session.transfer(flowFile, REL_SUCCESS)
  } catch (err) {
    log.error(err)
    session.transfer(flowFile, REL_FAILURE)
  }
}
